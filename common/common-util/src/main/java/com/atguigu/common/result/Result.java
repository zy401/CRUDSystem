package com.atguigu.common.result;

import lombok.Data;

/**
 * @创建人 ZY
 * @创建时间 2022/12/9
 * @描述
 */
@Data
public class Result<T> {
    //状态码
    private Integer code;
    //消息
    private String message;
    //数据
    private T data;

    public Result() {
    }

    //返回数据
    protected static <T> Result<T> build(T data) {
        Result<T> result = new Result<>();
        if (data != null) {
            result.setData(data);
        }
        return result;
    }
    //重载方法
    public static <T> Result<T> build(T data, Integer code,String message) {
        Result<T>result = build(data);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public static <T>Result<T>build(T data,ResultCodeEnum resultCodeEnum){
        Result<T> result = build(data);
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }
    /**
     * 操作成功
     */
    //重载方法
    public static <T>Result<T>ok(){
        return Result.ok(null);
    }

    public static <T>Result<T> ok(T data){
        Result<T> result = build(data);
        return build(data,ResultCodeEnum.SUCCESS);
    }

    /**
     * 操作失败
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(){
        return Result.fail(null);
    }
    public static <T> Result<T> fail(T data){
        Result<T> result = build(data);
        return build(data,ResultCodeEnum.FAIL);
    }

    public Result<T>message(String msg){
        this.setMessage(msg);
        return this;
    }

    public Result<T>code(Integer code){
        this.setCode(code);
        return this;
    }

}
