package com.atguigu.system.exception;

import com.atguigu.common.result.ResultCodeEnum;
import lombok.Data;

/**
 * @创建人 ZY
 * @创建时间 2022/12/9
 * @描述
 */
@Data
public class GuiguException extends RuntimeException {

    private Integer code;

    private String message;

    //创建异常类对象
    public GuiguException(Integer code,String message){
        super(message);
        this.code=code;
        this.message=message;
    }
    //枚举类对象
    public GuiguException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
        this.message = resultCodeEnum.getMessage();

    }

    @Override
    public String toString() {
        return "GuiguException{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
