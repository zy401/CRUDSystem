package com.atguigu.system.handler;

import com.atguigu.common.result.Result;
import com.atguigu.system.exception.GuiguException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @创建人 ZY
 * @创建时间 2022/12/9
 * @描述
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    //通用异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e){
        e.printStackTrace();
        return Result.fail();
    }

    //添加自定义异常

    @ExceptionHandler(GuiguException.class)
    @ResponseBody
    public Result error(GuiguException g){
        g.printStackTrace();
        return Result.fail().message(g.getMessage()).code(g.getCode());
    }
}
