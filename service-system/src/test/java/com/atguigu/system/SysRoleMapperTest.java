package com.atguigu.system;

import com.atguigu.model.system.SysRole;
import com.atguigu.system.mapper.SysRoleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @创建人 ZY
 * @创建时间 2022/12/9
 * @描述
 */
@SpringBootTest
public class SysRoleMapperTest {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Test
    public void testSelectList() {
        System.out.println("select-----");
        List<SysRole> sysRoles = sysRoleMapper.selectList(null);
        for (SysRole sysRole : sysRoles) {// iter快捷键
            System.out.println("sysRole = " + sysRole);  //soutv 快捷键
        }

    }

    @Test
    public void insertUser(){
        SysRole sysRole = new SysRole();
        sysRole.setRoleName("地狱管理员");
        sysRole.setDescription("地狱一部");
        int insert = sysRoleMapper.insert(sysRole);
    }

    @Test
    public void testQueryWrapper(){
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("description","尚硅谷");
        List<SysRole> sysRoles = sysRoleMapper.selectList(queryWrapper);
        for (SysRole sysRole : sysRoles) {
            System.out.println("sysRole = " + sysRole);

        }


    }
}
