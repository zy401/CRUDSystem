package com.atguigu.system.service;

import com.atguigu.model.system.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @创建人 ZY
 * @创建时间 2022/12/10
 * @描述
 */
public interface SysMenuService extends IService<SysMenu> {

    List<SysMenu> findNotes();
}
