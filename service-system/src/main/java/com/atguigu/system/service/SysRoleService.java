package com.atguigu.system.service;

import com.atguigu.model.system.SysRole;
import com.atguigu.model.vo.AssginRoleVo;
import com.atguigu.model.vo.SysRoleQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @创建人 ZY
 * @创建时间 2022/12/9
 * @描述
 */

public interface SysRoleService extends IService<SysRole> {
   IPage<SysRole> selectPage(Page<SysRole> pageParam, SysRoleQueryVo sysRoleQuery);


    Map<String, Object> getRolesByUserId(Long userId);

    void doAssign(AssginRoleVo assginRoleVo);
}
